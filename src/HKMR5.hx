package ;
import flash.filters.ConvolutionFilter;
import flash.geom.Point;
import refraction.core.ActiveComponent;
import refraction.core.Application;
import refraction.display.BlitComponentC;
import refraction.display.Surface2RenderComponentC;
import refraction.ds2d.LightSource;
import refraction.generic.PositionComponent;

/**
 * ...
 * @author worldedit
 */

class HKMR5 extends Weapon
{
	private var timer:Int;
	private var t:Int;
	private var canCast:Bool;
	
	public function new() 
	{
		timer = 3;
		super(2, "HKMR5");
	}

	override public function castWeapon(_position:PositionComponent):Void
	{
		
	}
	
	override public function setAnimation(_anim:Surface2RenderComponentC):Void
	{
		_anim.curAnimaition = 3;
		_anim.frame = 0;
	}
	
	override public function update():Void
	{
		if (t < 100)
		t++;
		if (t >= timer)
		{
			canCast = true;
		}
	}
	
	override public function persistCast(_position:PositionComponent):Void
	{
		if (!canCast)
		return;
		var p:Point = new Point(Application.mouseX / 2 + cast(Application.currentState, GameState).canvas.camera.x - _position.x, 
								Application.mouseY / 2 + cast(Application.currentState, GameState).canvas.camera.y - _position.y);
		p.normalize(400);
		var p2:Point = p.clone();
		p2.normalize(Math.random() * 10);
		p.x *= 400;
		p.y *= 400;
		var p3:Point = p.clone();
		p3.normalize(3);
		var px:Int = cast _position.x + 10 + p2.x - p3.y;
		var py:Int = cast _position.y + 10 + p2.y + p3.x;
		//Factory.createBullet(px, py, _position.x + p.x ,
		//					 _position.y + p.y, 20);
							 
		Factory.createFireball(px-5, py-5,_position.x + p.x-5,_position.y + p.y-5);
		var l:LightSource = new LightSource(cast px, py, 0xaaaa77,50);
		l.remove = true;
		cast(Application.currentState, GameState).shadowSystem.addLightSource(l);
		t = 0;
		canCast = false;
	}
}