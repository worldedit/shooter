package ;
import refraction.core.ActiveComponent;
import refraction.generic.PositionComponent;

/**
 * ...
 * @author worldedit
 */

class ItemComponent extends ActiveComponent
{
	
	public var position:PositionComponent;
	public var content:Weapon;

	public function new(_content:Weapon) 
	{
		super("item_comp");
		content = _content;
	}
	
	override public function load():Void 
	{
		position = cast entity.components.get("pos_comp");
	}
	
	public function collect(_inv:InventoryComponent):Void
	{
		var m:Weapon = content;
		_inv.currentWeapon = m;
		_inv.weapons[m.type] = m;
		_inv.setWeapon(m);
		m.getAmmo(_inv);
	}
	
}