package ;
import refraction.core.ActiveComponent;
import refraction.ds2d.LightSource;

/**
 * ...
 * @author qwerber
 */

class FireComponent extends ActiveComponent
{
	public var targetLight:LightSource;
	
	private var t:Int;
	private var timer:Int;
	
	public function new() 
	{
		super("fire_comp");
	}
	
	override public function load():Void 
	{
		timer = 60;
	}
	
	override public function update():Void 
	{
		t++;
		if (t >= timer)
		{
			entity.removeImmediately();
			targetLight.remove = true;
		}
		if (t < 40)
		{
			if (t < 20)
			targetLight.radius += 1;
			targetLight.v3Color.x += (1-targetLight.v3Color.x)/15;
			targetLight.v3Color.y += (0.6-targetLight.v3Color.y)/15;
			targetLight.v3Color.z -= 0.080;
		}
		if (t > 50)
		{
			targetLight.radius -= 2;
		}
	}
	
}