package ;
import flash.geom.Point;
import flash.ui.Keyboard;
import flash.Vector;
import refraction.core.ActiveComponent;
import refraction.core.Application;
import refraction.core.Utils;
import refraction.display.BlitComponentC;
import refraction.display.Surface2RenderComponentC;
import refraction.ds2d.LightSource;
import refraction.generic.PositionComponent;
import refraction.core.SubSystem;

/**
 * ...
 * @author worldedit
 */

class InventoryComponent extends ActiveComponent
{
	
	private var mouseDown:Bool;
	private var mouseWasDown:Bool;
	
	private var position:PositionComponent;
	private var swap:Bool;
	private var winq:ItemComponent;
	
	public var currentWeapon:Weapon;
	public var weapons:Vector<Weapon>;
	public var targetSystem:SubSystem<ItemComponent>; 
	
	public var wasDownE:Bool;

	public var blitc:Surface2RenderComponentC;
	
	public var targetFlashLight:LightSource;
	
	public var lightT:Int;
	public var lightFullTime:Int;
	public var lightDimmingTime:Int;
	public var dimProgressionTemp:Float;
	
	public var primaryAmmo:AmmunitionObject;
	public var secondaryAmmo:AmmunitionObject;
	
	public var currentAmmo:AmmunitionObject;
	
	private var lightControl:LightControlComponent;
	
	public function new() 
	{
		super("inventory_comp");
		//currentWeapon = new M357();
		weapons = new Vector<Weapon>();
		weapons.push(null);
		weapons.push(null);
		weapons.push(null);
		weapons.push(null);
	}
	
	override public function load():Void 
	{
		lightFullTime = 200;
		lightDimmingTime = 800;
		dimProgressionTemp = 1 / lightDimmingTime;
		lightT = lightFullTime + lightDimmingTime;
		position = cast entity.components.get("pos_comp");
		lightControl = cast entity.components.get("light_control_comp");
		
		secondaryAmmo = new AmmunitionObject(6);
		secondaryAmmo.applyAmmo(10);
		secondaryAmmo.reload();
		
		currentAmmo = secondaryAmmo;
	}
	
	public inline function setWeapon(_weapon:Weapon):Void
	{
		_weapon.setAnimation(blitc);
	}
	
	override public function update():Void 
	{		
		var i:Int = targetSystem.components.length;
		while (i-->0)
		{
			var item:ItemComponent = targetSystem.components[i];
			swap = false;
			winq = null;
			if (Utils.posDis2(position, item.position) < 100)
			{
				if (weapons[item.content.type] != null)
				{
					swap = true;
					winq = item;
				}else{
					item.entity.removeImmediately();
					item.collect(this);
				}
			}
		}
		
		if (Application.keys.get(Keyboard.E) && !wasDownE == true && winq != null)
		{
			Factory.createItem(currentWeapon.name, cast position.x, cast position.y);
			swap = false;
			
			winq.entity.removeImmediately();
			winq.collect(this);
			
			winq = null;
		}
		
		mouseWasDown = mouseDown;
		mouseDown = Application.mouseIsDown;
		if (currentWeapon != null)
		{
			currentWeapon.update();
			if (mouseDown)
			{
				if (!mouseWasDown)
				{
					currentWeapon.castWeapon(position);
				}
				currentWeapon.persistCast(position);
			}
		}
		
		wasDownE = Application.keys.get(Keyboard.E);
		
		lightT --;
		if (lightT < lightDimmingTime)
		{
			var rat:Float = lightT / lightDimmingTime * (lightControl.targetLight.color/0xffffff);
			lightControl.setColor(rat, rat, rat);
		}
	}
}